# **RedHat CloudForms integration with RedHat Enterprise Virtualization**

There is a great number possibilities for rich customization while integrating RH CloudForms and RHV. This repo contains a datastore domain called "Automate". Within this domain there are several Namespaces that allow to configure the below use cases with a minimal effort.

- Used Versions:
    - RH CloudForms v4.7
    - Red Hat Virtualization v4.2

##  Requirements

- To have installed and configured a CFME appliance
- To have a Red Hat Virtualization instance integrated in CloudForms
- To have at least one VM template deployed in RHV


##  Integrating Procedure

Login to your CFME appliance and navigate to:  **_Automation >  Automate > Import/Export_**

- Edit the fields as follows:
    - **Git URL:** https://gitlab.com/rcardona/cf_rhv_statemachines

Leave the rest of the fields unchanged and click Submit

![screen shot 0](images/integratingDomain_0.png "Import/Export")

It shall appear the bellow window, just click submit and it will be all settled.

![screen shot 1](images/integratingDomain_1.png "Import/Export")

Navigate to:  **_Automation >  Automate > Explorer_**, the Automate domain should appear as below:

![screen shot 2](images/integratingDomain_2.png "Datastore Domains")

-----------------------------------------------------------------

## Use case 1 (starting)

Objective: to create a _Service Catalog Item_ that will prompt existing VLANs in RHV and set the desire one for the provisioning request.

### Steps:

-----------------------------------------------------------------

1- Create the _Service Dialog_. Navigate to:  **_Automation >  Automate > Customization > Service Dialogs_**

It should at least have four fields with the below configuration

Field 1 : **RHV Provider**

Type: Dropdown

- Data:
  - **Field Information**
    - **Label:** RHV Provider
    - **Name:** rhv_ems
    - **Dynamic:** Yes
 
  - **Options**
    - **Entry Point:** ServiceDialog/Class/rhv_providers_listing
    - **Show Refresh Button:** Yes
    - **Load values on init:** Yes
    - **Required:** Yes
    - **Value type:** String
    - **Multiselect:** No
    - **Fields to refresh:** Datacenter

No change is needed in the **Advance** or **Overridable Options** section

![screen shot 3](images/serviceDialog_field_1_0.png)

![screen shot 4](images/serviceDialog_field_1_1.png)

-----------------------------------------------------------------

Field 2 : **Datacenter**

Type: Dropdown

- Data:
  - **Field Information**
    - **Label:** Datacenter
    - **Name:** rhv_ems_datacenter
    - **Dynamic:** Yes
 
  - **Options**
    - **Entry Point:** ServiceDialog/Class/datacenters_listing
    - **Show Refresh Button:** Yes
    - **Load values on init:** No
    - **Required:** Yes
    - **Value type:** String
    - **Multiselect:** No
    - **Fields to refresh:** Nothing Selected

No change is needed in the **Advance** or **Overridable Options** sections

-----------------------------------------------------------------

Field 3 : **VM Name**

Type: Text Box

- Data:
  - **Field Information**
    - **Label:** VM Name
    - **Name:** vm_name
    - **Dynamic:** No
 
  - **Options**
    - **Default value:** 
    - **Protected:** No
    - **Required:** Yes
    - **Read only:** No
    - **Visible:** Yes
    - **Value type:** Nothing Selected
    - **Validation:** No
    - **Fields to refresh:** Nothing Selected


No change is needed in the **Advance** section

-----------------------------------------------------------------

Field 4 : **Network**

Type: Dropdown

- Data:
  - **Field Information**
    - **Label:** Network
    - **Name:** rhv_vnic_profile
    - **Dynamic:** Yes
 
  - **Options**
    - **Entry Point:** ServiceDialog/Class/vnic_profile_listing
    - **Show Refresh Button:** Yes
    - **Load values on init:** No
    - **Required:** Yes
    - **Value type:** String
    - **Multiselect:** No
    - **Fields to refresh:** Nothing Selected

No change is needed at the **Advance** or **Overridable Options**

-----------------------------------------------------------------

2- Create the Service Catalog Item. Navigate to:  **_Services >  Catalogs > Catalogs_**

- First create a new Catalog, by clicking in the **_configuration_** button, then follow and complete the form.

    - **Name:** RHV

![screen shot 5](images/catalogCreation.png)

- Once the Catalog is available, proceed to create the Service Catalog Item, Navigate to:  **_Services >  Catalogs > Catalogs Items_**

![screen shot 6](images/catalogItemCreation.png)

 It should have the following configuration:
 
 - Adding a new Service Catalog Item
    - **Catalog Item Type:** Red Hat Virtualization

    **=>**
  
- Basic Info:
    - **Name / Description:** RHV VM Provisioning  /  RHV VM Provisioning  (Tick **Display in Catalog**)
    - **Catalog:** My Company/RHV
    - **Dialog:** rhv_vm_provision_dialog

- Details:
    No configuration needed

- Request Info:
  
    - First select a VM Template
    - Set all the **Required** fields (The network will be overwritten with the information from the Service Dialog)
    - **VM Name:** changeme

-----------------------------------------------------------------

### The Service Catalog Item should look like...

![screen shot 7](images/serviceCatalogItem.png)


## Use case 1 (end)
-----------------------------------------------------------------