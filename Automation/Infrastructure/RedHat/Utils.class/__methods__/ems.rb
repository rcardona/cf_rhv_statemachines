#
# Utility library for Red Hat Virtualization to retrive existing providers
#

module Automation
  module Infrastructure
      module RedHat
        class Utils

          def rhv_providers
            providers = {}
			providers['!'] = '-- select from list --'
			providers_inventory = $evm.vmdb(:Ems).all

			providers_inventory.each do |this_provider|
  				next if this_provider.type != "ManageIQ::Providers::Vmware::InfraManager::Vm"
    				providers[this_provider.id] = this_vm.name
            	end
            providers
          end
            
      end
    end
  end
end
