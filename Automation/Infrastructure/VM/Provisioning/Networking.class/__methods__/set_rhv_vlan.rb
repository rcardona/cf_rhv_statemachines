begin
  prov = $evm.root["miq_provision"]
  vnic_profile = prov.get_option(:rhv_vnic_profile)
  
  $evm.log(:info, "Using vnic_profile : #{vnic_profile}")
  
  prov.set_vlan(vnic_profile)

 rescue => err
  $evm.log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_STOP

end
