begin
  rhv_ems = $evm.vmdb(:ems).where(:type => 'ManageIQ::Providers::Redhat::InfraManager')
  values_hash = {}
  values_hash['!'] = '-- select from list --'

  unless rhv_ems.empty?
      rhv_ems.each do | existing_rhv_provider |
      values_hash[existing_rhv_provider[:id]] = existing_rhv_provider[:name]
    end
  end

  list_values = {
    'sort_by'    => :value,
    'data_type'  => :string,
    'required'   => true,
    'values'     => values_hash
  }
  list_values.each { |key, value| $evm.object[key] = value }

 rescue => err
  $evm.log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_STOP

end
