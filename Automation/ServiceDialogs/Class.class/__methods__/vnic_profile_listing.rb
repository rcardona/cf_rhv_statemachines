begin
  rhv_ems = $evm.root['dialog_rhv_ems']
  rhv_ems_datacenter = $evm.root['dialog_rhv_ems_datacenter']
  values_hash = {}
  values_hash['!'] = '** no vNIC profiles found **'

  # Call the embedded method
  rhv  = Automation::Infrastructure::RedHat::Utils.new(rhv_ems)
  
  $evm.log(:info, "culito rhv_ems : #{rhv_ems}")
  $evm.log(:info, "culito rhv_ems_datacenter : #{rhv_ems_datacenter}")
  
  vnic_profiles = rhv.vnic_profiles(rhv_ems_datacenter)
  
  unless vnic_profiles.empty?
    values_hash['!'] = '-- select from list --'
      vnic_profiles.each do | profile |
      values_hash[profile[:id]] = profile[:name]
    end
  end

  list_values = {
    'sort_by'    => :value,
    'data_type'  => :string,
    'required'   => true,
    'values'     => values_hash
  }
  list_values.each { |key, value| $evm.object[key] = value }

 rescue => err
  $evm.log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_STOP

end
