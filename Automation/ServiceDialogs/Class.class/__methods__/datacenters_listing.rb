begin
  rhv_ems_id = $evm.root['dialog_rhv_ems']
  datacenters = $evm.vmdb(:datacenter).where(:ems_id => rhv_ems_id)
  values_hash = {}
  values_hash['!'] = '-- select from list --'

  unless datacenters.empty?
      datacenters.each do | existing_datacenter |
      values_hash[existing_datacenter[:name]] = existing_datacenter[:name]
    end
  end

  list_values = {
    'sort_by'    => :value,
    'data_type'  => :string,
    'required'   => true,
    'values'     => values_hash
  }
  list_values.each { |key, value| $evm.object[key] = value }

 rescue => err
  $evm.log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_STOP

end
